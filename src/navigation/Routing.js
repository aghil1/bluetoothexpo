import React, {useEffect, useState, useRef} from 'react'
import {NavigationContainer, CommonActions} from '@react-navigation/native'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import NotificationManager from '../utils/NotificationManager';

import Home from '../screen/Home';
import Detail from '../screen/Detail';
import Barang from '../screen/Barang';

const Stack = createNativeStackNavigator();
const navigationRef = React.createRef();

export default function Routing() {
   const routeNameRef = useRef();
   const {data, token} = NotificationManager();
   const [navigationReady, setNavigationReady] = useState(false);

   const onNavigatorReady = () => {
		try {
			routeNameRef.current = navigationRef.current.getCurrentRoute().name;
			routingInstrumentation.registerNavigationContainer(navigationRef);
		} catch (err) {
			console.log();
		}
	};

   const onNavigatorContainerStateChange = () => {
		try {
			const currentRouteName = navigationRef?.current.getCurrentRoute() === undefined ? '' : navigationRef.current.getCurrentRoute().name;

			routeNameRef.current = currentRouteName;
		} catch (e) {
			console.log('error navigation');
		}
	};

   const handleNotif = () => {
      let dataIdFromNotif = data?.notification?.request?.content?.data?.message?.id;
      let screenName = data?.notification?.request?.content?.data?.message?.screen;
      const navigation = navigationRef.current;
      switch (screenName) {
         case 'detail':
            navigation.dispatch({
               ...CommonActions.reset({
                  index: 0,
                  routes: [
                     {
                        name: 'Home',
                     },
                     {
                        name: 'Detail',
                        params: {
                           idBarang: dataIdFromNotif,
                        },
                     },
                  ],
               }),
            });
            break;
         case 'barang':
            navigation.dispatch({
               ...CommonActions.reset({
                  index: 0,
                  routes: [
                     {
                        name: 'Home',
                     },
                     {
                        name: 'Barang',
                        params: {
                           idBarang: dataIdFromNotif,
                        },
                     },
                  ],
               }),
            });
            break;
      
         default:
            break;
      }

   }

   useEffect(() => {
      if (typeof data !== 'string') {
         handleNotif();
      }
   },[data])

   return(
      <NavigationContainer
         ref={navigationRef}
         onStateChange={onNavigatorContainerStateChange}
         onReady={() => {
            onNavigatorReady();
            setNavigationReady(true);
         }}
      >
         <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Detail" component={Detail} />
            <Stack.Screen name="Barang" component={Barang} />
         </Stack.Navigator>
      </NavigationContainer>
   )
}