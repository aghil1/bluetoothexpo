import React from "react";
import {
   View,
   Text
} from 'react-native';

const Detail = ({
   navigation,
   route
}) => {
   console.log('cek route detail', route);
   return(
      <View style={{flex: 1, backgroundColor: '#fff'}}>
         <View style={{
            width: '100%',
            paddingVertical: 15,
            backgroundColor: 'cyan',
            justifyContent: 'center',
            alignItems: 'center'
         }}>
            <Text>Detail Screen</Text>
         </View>
         <Text>Id Barang : {route.params ? route.params.idBarang : 'BRX-000'}</Text>
      </View>
   )
}

export default Detail;