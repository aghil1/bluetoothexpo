import React, {useEffect, useState} from 'react';
import {
   View,
   Text,
   TouchableOpacity,
   ToastAndroid,
   StyleSheet
} from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import * as Clipboard from 'expo-clipboard'
import NotificationManager from '../utils/NotificationManager';

const Home = ({
   navigation,
   route
}) => {
   const [tokenfcm, setTokenFcm] = useState('')
   const {data, token} = NotificationManager();
   const getToken = async () => {
      const token = await AsyncStorage.getItem('tokenFCM');
      setTokenFcm(token)
   }

   const copyToClipboard = async () => {
      await Clipboard.setStringAsync(token ? token.data : 'token failed');
      ToastAndroid.show('Token telah disalin', ToastAndroid.SHORT);
   };

   useEffect(() => {
      console.log('cek token fcm', token);
      getToken();
   },[])

   return(
      <View style={styles.container}>
         <Text style={{fontSize: 14, fontWeight: 'bold'}}>{token?.data}</Text>
         <TouchableOpacity
            onPress={copyToClipboard}
            style={styles.btnCopy}
         >            
            <Text style={{color: '#fff', fontWeight: '600'}}>Salin</Text>
         </TouchableOpacity>
         <TouchableOpacity
            onPress={() => navigation.navigate('Detail')}
            style={styles.btnCopy}
         >            
            <Text style={{color: '#fff', fontWeight: '600'}}>Go To Detail</Text>
         </TouchableOpacity>
      </View>
   )
}

const styles = StyleSheet.create({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#fff'
   },
   btnCopy: {
      width: 130,
      paddingVertical: 10,
      borderRadius: 6,
      backgroundColor: '#43a047',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20
   }
})

export default Home;